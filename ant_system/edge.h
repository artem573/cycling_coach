#ifndef EDGE_H
#define EDGE_H

#include "consts.h"
#include "node.h"

struct node;

struct edge
{
  int nodes[2];
  double length = 0;

  int index = 0;

  double pher = pher_0;
 
  void set_edge(node& n1, node& n2);
};

#endif // EDGE_H
