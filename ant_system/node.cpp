#include "node.h"

void node::update()
  {
    double sum = 0;

    for(int i = 0; i < nbr_edges.size(); i++)
        sum += pow(edges[nbr_edges[i]].pher,alpha)
              *pow(1.0/edges[nbr_edges[i]].length,beta);

    for(int i = 0; i < nbr_edges.size(); i++)
        decision_table[i] = pow(edges[nbr_edges[i]].pher,alpha)
                           *pow(1.0/edges[nbr_edges[i]].length,beta)/sum;

  }
