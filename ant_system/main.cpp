#include <math.h>
#include <iostream>
#include <vector>
#include "consts.h"
#include "edge.h"
#include "node.h"
#include <ctime>
#include "ant.h"


int main()
{
  std::srand(std::time(0));
  double l = 1;
  
  // Set up nodes and edges
  node nodes[N];
  edge edges[N];

  for(int i = 0; i < N; i++)
    {
      nodes[i].index = i;
      edges[i].index = i;
      nodes[i].edges = edges;
    }


  nodes[0].x = 0;
  nodes[0].y = 0;
  nodes[1].x = nodes[0].x + l;
  nodes[1].y = nodes[0].y;
  nodes[2].x = nodes[1].x + l*cos(PI/4);
  nodes[2].y = nodes[1].y + l*sin(PI/4);
  nodes[3].x = nodes[1].x + l*cos(PI/4);
  nodes[3].y = -2*nodes[2].y;
  nodes[4].x = nodes[2].x + l*cos(PI/4);
  nodes[4].y = nodes[1].y;
  nodes[5].x = nodes[4].x + l;
  nodes[5].y = nodes[4].y;
  
  edges[0].set_edge(nodes[0],nodes[1]);
  edges[1].set_edge(nodes[1],nodes[2]);
  edges[2].set_edge(nodes[1],nodes[3]);
  edges[3].set_edge(nodes[2],nodes[4]);
  edges[4].set_edge(nodes[3],nodes[4]);
  edges[5].set_edge(nodes[4],nodes[5]);

  for(int i = 0; i < N; i++)
    nodes[i].update();
  

  /*
  for(int i = 0; i < N; i++)
      std::cout << edges[i].length << " / " << edges[i].nodes[0]
                << ", " << edges[i].nodes[1] << "\n";

  for(int i = 0; i < N; i++)
    {
      std::cout << nodes[i].index << ": \n";
      std::cout << "nbr_edges: ";
      for(int edge_i = 0; edge_i < nodes[i].nbr_edges.size(); edge_i++)
          std::cout << nodes[i].nbr_edges[edge_i] << " ";
      std::cout << "nbr_nodes: ";
      for(int node_i = 0; node_i < nodes[i].nbr_nodes.size(); node_i++)
        {
          std::cout << nodes[i].nbr_nodes[node_i] << " "
                    << nodes[i].decision_table[node_i] << " / ";
        }
      std::cout << "\n\n";
    }
  */
  
  ant ants[N_ants];
  
  for(int i_ant = 0; i_ant < N_ants; i_ant++)
    {
      ants[i_ant].edges = edges;
      ants[i_ant].nodes = nodes;
    }

  for(int t = 0; t < N_t; t++)
    {
      int iter = 0;

      for(int i_ant = 0; i_ant < N_ants; i_ant++)
        ants[i_ant].scatter();

      int dead_ants = 0;
      while (dead_ants < N_ants and iter < 100)
        {
          dead_ants = 0;
          iter++;

          for(int i_ant = 0; i_ant < N_ants; i_ant++)
            {
              if(not ants[i_ant].dead)
                  ants[i_ant].move();
              else
                  dead_ants ++;
            }
        }

      for(int i = 0; i < N; i++)
        edges[i].pher *= (1-rho);

      for(int i_ant = 0; i_ant < N_ants; i_ant++)
        {
          ants[i_ant].update();
          ants[i_ant].reload();
        }

      for(int i = 0; i < N; i++)
        {
          nodes[i].update();
          std::cout << edges[i].pher << "; ";
        }
      std::cout << "\n";
    }
  
  return 0;
}
