#ifndef CONSTS_H
#define CONSTS_H

extern const double PI, pher_0, alpha, beta, rho;
extern const int N, N_ants, N_t;

#endif // CONSTS_H
