#include "consts.h"

const double PI = 3.14;
const int N = 6;
const int N_ants = 10;
const int N_t = 20;
const double pher_0 = 1e-5;
const double alpha = 1;
const double beta = 5;
const double rho = 0.5;

