#ifndef NODE_H
#define NODE_H

#include <math.h>
#include <vector>
#include "edge.h"

struct edge;

struct node
{
  double x = 0;
  double y = 0;

  int index = 0;

  std::vector<int> nbr_edges;
  std::vector<int> nbr_nodes;

  std::vector<double> decision_table;

  edge* edges;

  void update();
};

#endif // NODE_H
