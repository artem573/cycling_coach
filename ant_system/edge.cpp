#include "edge.h"

void edge::set_edge(node& n1, node& n2)
  {
    nodes[0] = n1.index;
    n1.nbr_edges.push_back(index);
    n1.nbr_nodes.push_back(n2.index);
    n1.decision_table.push_back(0);
    nodes[1] = n2.index;
    n2.nbr_edges.push_back(index);
    n2.nbr_nodes.push_back(n1.index);
    n2.decision_table.push_back(0);
    
    length = sqrt(pow(n1.x-n2.x,2) + pow(n1.y-n2.y,2));
  }
