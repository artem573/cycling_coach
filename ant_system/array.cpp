#include "array.h"

template <class T>
array<T>::array()
{
    size_ = 0;
}

template <class T>
array<T>::array(int size)
{
    size_ = size;
    p_ = new T [size_];
}

template <class T>
array<T>::~array()
{
    if(size_ > 0) delete[] p_;
}

template <class T>
void array<T>::add(T value)
{

  if(size_ > 0)
    {
      T* temp = new T(size_);
      for (int i = 0; i < size_; i++)
        temp[i] = p_[i];

      delete[] p_;

      p_ = new T[size_+1];
      for (int i = 0; i < size_; i++)
        p_[i] = temp[i];
      delete[] temp;
    }
  else
    p_ = new T[1];

  p_[size_] = value;
  size_++;

}

template <class T>
void array<T>::clear()
{
  size_ = 0;
  delete[] p_;
}


template <class T>
T& array<T>::operator [](int i)
{
//    assert( i>=0 && i<dimX_ );
    return p_[i];
}

template <class T>
T array<T>::operator [](int i) const
{
//    assert( i>=0 && i<dimX_ );
    return p_[i];
}


template class array<int>;
template class array<double>;
