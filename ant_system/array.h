#ifndef ARRAY_H
#define ARRAY_H

#include <iostream>
#include <math.h>
#include <assert.h>

template <class T>
class array
{
private:
    T* p_;
    int size_;

public:
    array();
    array(int size);
    ~array();

    void add(T value);
    void clear();
    
    T&        operator[](int i);
    T         operator[](int i) const;
    int       size() const {return size_;}
    int       null() const {return size_ == 0;}
};

typedef array<int> array_int;
typedef array<double> array_doub;

#endif // ARRAY_H
