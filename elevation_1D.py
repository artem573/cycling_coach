import math
import random
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.mlab as ml

N = 100
start_freq = 1
N_freq = 3
exp = -0.5

def noise(x):
    res = [0.]*len(x)
    for freq in range(1,N_freq+1):
        phase = random.uniform(0, 2*math.pi)
        for i in range(len(x)):
            res[i] += pow(freq,exp)*math.sin(2*math.pi * freq*x[i]/len(x) + phase)


    return res

x = [i for i in range(N)]

plt.plot(x,noise(x))
plt.show()
