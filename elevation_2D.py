import math
import random
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate

random.seed()

# Map size
length = 10 # [km]
width = 10 # [km]
altitude_max = 400 # [m]

N_nodes = 150 # The number of nodes

# The number of points for elevation generator
N = 50
Nx, Ny = N, N

# The noise generator parameters
start_freq = 1
N_freq = 3
exp = -0.5


class Point:
    def __init__(self):
        self.x = 0.
        self.y = 0.

    def __init__(self,x,y):
        self.x = x
        self.y = y
        

def noise(x,y,z):
    for freq in range(1,N_freq+1):
        phase_x, phase_y = random.uniform(0, 2*math.pi), random.uniform(0, 2*math.pi)
        for i in range(Nx+1):
            for j in range(Ny+1):
                index = i*(Ny+1) + j
                z[index] += pow(freq,exp)*math.sin(2*math.pi * freq*index/Nx + phase_x)\
                          + pow(freq,exp)*math.sin(2*math.pi * freq*j/Ny + phase_y)

def min_path(l,l_max):
    l_min = l_max
    index = -1
    for k in range(len(l)):
        if l_min >= l[k]:
            l_min = l[k]
            index = k
    return index, l_min

# Check if segments have intersection
def intersect(A,B,C,D):
    def ccw(A,B,C):
        return (C.y-A.y) * (B.x-A.x) > (B.y-A.y) * (C.x-A.x)

    if (A.x==C.x and A.y==C.y) or (A.x==D.x and A.y==D.y) or\
       (B.x==C.x and B.y==C.y) or (B.x==D.x and B.y==D.y): return False
    return ccw(A,C,D) != ccw(B,C,D) and ccw(A,B,C) != ccw(A,B,D)


def main():
    # Create nodes and edges
    x_nodes = np.random.rand(N_nodes)
    y_nodes = np.random.rand(N_nodes)
    altitude_nodes = np.zeros(N_nodes)

    for n in range(N_nodes):
        x_nodes[n] *= length
        y_nodes[n] *= width

    # Creating edges
    edges = []
    edges_length = []
    edge_nodes_index = []

    allowed_edges = [2]*N_nodes
    for i in range(N_nodes):
        allowed_edges[i] = random.randint(2,4)

    for pnt1 in range(N_nodes):
        pntA = Point(x_nodes[pnt1], y_nodes[pnt1])

        l = np.zeros(N_nodes)
        l_max = 0
        # Get edges length
        for point in range(N_nodes):
            pnt = Point(x_nodes[point], y_nodes[point])
            l[point] = math.sqrt(pow(pntA.x-pnt.x,2) + pow(pntA.y-pnt.y,2))
            if l_max < l[point]: l_max = l[point]
        l[pnt1] = l_max
        
        while allowed_edges[pnt1] > 0:
            pnt2, size = min_path(l,l_max)
            if pnt2 == -1 or pnt2 == pnt1: break
            l[pnt2] = l_max+1
            pntB = Point(x_nodes[pnt2], y_nodes[pnt2])

            # Remove an edge(pnt1,pnt2) from the list if there is already 
            # an edge(pnt2,pnt1)
            existed_edge = False
            for edge in range(len(edge_nodes_index)):
                pnt3, pnt4 = edge_nodes_index[edge]
                if pnt1 == pnt4 and pnt2 == pnt3: existed_edge = True
            

            # Remove an edge from the list if there is a third point nearby
            # located inside circle created by edge as diameter
            # and closer to the edge than edge length multiplied by a factor (e.g. 0.2)
            near_points = False

            line_k = (pntB.y-pntA.y)/(pntB.x-pntA.x)
            line_b = pntB.y - line_k*pntB.x
            radius_2 = pow(0.5*size,2)
            pntCenter = Point(0.5*(pntA.x + pntB.x), 0.5*(pntA.y + pntB.y))
            for pnt3 in range(N_nodes):
                if pnt3 != pnt1 and pnt3 != pnt2:
                    pntC = Point(x_nodes[pnt3], y_nodes[pnt3])
                    distance = abs(-line_k*pntC.x + pntC.y - line_b)/math.sqrt(pow(-line_k,2) + 1)
                    if pow(pntC.x-pntCenter.x,2) + pow(pntC.y-pntCenter.y,2) < radius_2 and distance < 0.2*size:
                        near_points = True

            # Remove an edge from the list if it intersects with the existing ones
            is_intersect = False
            for edge in range(len(edge_nodes_index)):
                pnt3, pnt4 = edge_nodes_index[edge]
                pntC, pntD = Point(x_nodes[pnt3],y_nodes[pnt3]), Point(x_nodes[pnt4],y_nodes[pnt4])
                if intersect(pntA,pntB,pntC,pntD): is_intersect = True

            if not near_points and not existed_edge and not is_intersect and allowed_edges[pnt2] > 0:
                edges.append([x_nodes[pnt1],x_nodes[pnt2]])
                edges.append([y_nodes[pnt1],y_nodes[pnt2]])
                edges.append('black')
                edges_length.append(size)
                edge_nodes_index.append([pnt1, pnt2])
                allowed_edges[pnt1] -= 1
                allowed_edges[pnt2] -= 1




    # Create arrays filled with "unstructured" data for map generation
    x = np.zeros((Nx+1)*(Ny+1))
    y = np.zeros((Nx+1)*(Ny+1))
    z = np.zeros((Nx+1)*(Ny+1))

    for i in range(Nx+1):
        for j in range(Ny+1):
            index = i*(Ny+1) + j
            x[index] = i*length/Nx
            y[index] = j*width/Ny

    noise(x,y,z)
    z_min = z.min()
    z_max = z.max()

    # Scale elevation levels according to the max altitude given
    for i in range(Nx+1):
        for j in range(Ny+1):
            index = i*(Ny+1) + j
            z[index] = (z[index] - z_min)/(z_max-z_min)*altitude_max

    # Interpolate altitudes to random scattered nodes
    altitude_nodes = scipy.interpolate.griddata((x, y), z, (x_nodes, y_nodes), method='linear')

    # Set up a regular grid of interpolation points
    xi, yi = np.linspace(x.min(), x.max(), 10), np.linspace(y.min(), y.max(), 10)
    xi, yi = np.meshgrid(xi, yi)
    zi = scipy.interpolate.griddata((x, y), z, (xi, yi), method='linear')

    plt.contour(xi, yi, zi, 10, linewidths=0.5, colors='k')
    plt.contourf(xi, yi, zi, 50, cmap='rainbow')

    plt.colorbar()
    plt.scatter(x_nodes, y_nodes, marker='o', s=50, zorder=10)
    for i in range(N_nodes):
        plt.annotate(i, (x_nodes[i],y_nodes[i]))
    plt.plot(*edges)
    plt.show()

    
    with open('graph', 'w+') as f_graph:
        f_graph.write('nodes: x [km], y [km], altitude [m]\n')
        for node in range(N_nodes):
            f_graph.write(str(x_nodes[node]) + ', ' + str(y_nodes[node]) + ', ' + str(altitude_nodes[node]) + '\n')
        f_graph.write('edges\n')
        for edge in range(len(edge_nodes_index)):
            f_graph.write(str(edge_nodes_index[edge][0]) + ', ' + str(edge_nodes_index[edge][1]) + '\n')
                        

if __name__ == '__main__':
    main()
